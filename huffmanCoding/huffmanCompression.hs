module HuffmanCompression
(
  compressByteString
, decompressByteString
)
where

import Data.List
import Data.Char
import Control.Applicative
import Control.Monad.State
import Data.Word
import Data.Array
import qualified Data.Map.Strict as DMap
import qualified Data.ByteString.Lazy as BS
import qualified Data.ByteString.Lazy.Char8 as Char8
import qualified Data.Binary.Builder as Bin
import qualified Data.Binary.Get     as BinGet

type BitString = [Bool]

{- 
type Symbol = Int
    
    A type to represent the potential values that we need to encode.
    In total, there are 257 possible values that might be encoded, 
    i.e. 256 possible values that a byte might be assigned to, 
    plus one special value representing the end-of-stream.
-}
type Symbol = Int
type Frequency = Int
type ByteFrequency = (Word8, Int)
type SymbolFrequency = (Symbol, Int)
type SymbolEncoding = (Symbol, BitString)

{-
type CodeMap = Array Int BitString
    A type to store the encoding associated with each symbol.
-}
type CodeMap = Array Int BitString

word8_to_int = fromIntegral::Word8->Int
word64_to_int = fromIntegral::Word64->Int
int_to_word8 = fromIntegral::Int->Word8
int_to_word64 = fromIntegral::Int->Word64

endOfStream :: Symbol   -- special symbol to mark the end of encoded data
endOfStream = 256

{- 
compressByteString :: BS.ByteString -> BS.ByteString
    
    Compresses a bytestring by applying huffman coding, endoding
    one byte at a time. There are 257 possible values to be encoded, 
    i.e. 256 possible values that a byte might be assigned to, 
    plus one special value representing the end-of-stream.
    
    The returned bytestring consists of:
      - The header, i.e. a sequence of byte-frequency pairs that will
        be used for reconstructing the huffman tree when decompressing.
        The end of the header is marked by a 0-0 pair.
      - The content, i.e. the bytes of the original byte string followed
        by a special end-of-string symbol, all encoded using huffman coding. 
        The encoding may produce a sequence of bits whose length is not 
        multiple of 8. In that case the sequence is padded with zeros, 
        to achieve a length multiple of 8, which is convertible to byte string.
-}
compressByteString :: BS.ByteString -> BS.ByteString
compressByteString bs = 
    let byteFrequencies     = DMap.toList $ countBytes bs
        symbolFrequencies   = (map (\(w,i) -> (word8_to_int w, i)) byteFrequencies) 
                              ++ [(endOfStream, 1)]
        huffmanCodeMap      = fst . generateHuffmanCodeMap $ symbolFrequencies
        endOfStreamBits     = encodeSymbol huffmanCodeMap endOfStream
        encodedContent      = toByteString  . (++endOfStreamBits)
                                            . encodeByteString huffmanCodeMap 
                                            $ bs
        encodedHeader       = createHeader byteFrequencies
    in  BS.append encodedHeader encodedContent
    
    
{- 
decompressByteString :: BS.ByteString -> Maybe BS.ByteString
    
    Receives a byte string and tries to decompress, assuming it is of the
    expected format. If the string is not of the expected format Nothing
    is returned. The decompression is done in the following steps:
      - The header is extracted from the byte string, i.e. a sequence of 
        byte-frequency pairs  that will be used for reconstructing the huffman tree.
      - A huffman tree is built using the byte-frequency pairs.
      - Content is extracted from the byte string and decoded using the huffman tree.
-}    
decompressByteString :: BS.ByteString -> Maybe BS.ByteString
decompressByteString bs = do
    byteFrequencies <- extractHeader bs
    let symbolFrequencies = map (\(b,f) -> (word8_to_int b,f)) byteFrequencies
    content <- extractContent bs
    let huffmanTree = snd . generateHuffmanCodeMap $ symbolFrequencies ++ [(endOfStream, 1::Int)]
    return $ decodeByteString huffmanTree content
   
   
{-
extractHeader :: BS.ByteString -> Maybe [SymbolFrequency]
    
    Tries to extract the header of the bytestring, i.e. a sequence of 
    (byte,frequency) pairs stored as (Word8,Word64). Therefore 9 bytes
    at a time are read, until (0,0) is encountered. If (0,0) is not found
    before the end of the string, then the string is of invald format.
-}        
extractHeader :: BS.ByteString -> Maybe [ByteFrequency]
extractHeader bs =
    if BS.length bs < 9 then 
        Nothing
    else
        case BS.all (==0) . BS.take 9 $ bs of
            -- 9 consecutive bytes of 0 mark the end of the header
            True  -> Just []

            -- First 9 bytes, toghether with the result of the recursive call
            -- are part of the header.
            False -> (++) <$> Just [(BS.head bs, getFrequency bs)]
                          <*> (extractHeader $ BS.drop 9 bs)

            where   -- getFrequency : skip the 1st byte and read frequency as a 8 byte int
                    getFrequency :: BS.ByteString -> Int
                    getFrequency = word64_to_int . BinGet.runGet BinGet.getWord64le . BS.drop 1

                    
{- 
extractContent :: BS.ByteString -> Maybe BS.ByteString

    Skips the header of the bytestring, and returns the rest. 
-}
extractContent :: BS.ByteString -> Maybe BS.ByteString
extractContent bs =
    if BS.length bs < 9 then 
        Nothing
    else
        case BS.all (==0) . BS.take 9 $ bs of
            -- True -> 9 bytes set to 0 i.e. end of the header was found, 
            -- the rest of the byte string is the content
            True -> Just $ BS.drop 9 bs 

            -- False -> End of header not found yet, skip the first 9 bytes and repeat
            False -> extractContent $ BS.drop 9 bs

            
{-
decodeByteString :: HuffmanTreeNode -> BS.ByteString -> BS.ByteString

    Decodes the bit-sequence of the given bytestring, using the provided
    huffman tree, to produce a new bytestring.
-}
decodeByteString :: HuffmanTreeNode -> BS.ByteString -> BS.ByteString
decodeByteString huffmanTree = decodeBitString huffmanTree . fromByteString


{-
decodeBitString :: HuffmanTreeNode -> BitString -> BS.ByteString
    
    Decodes the given bit sequence, using the provided huffman tree,
    to produce a byte string.
-}
decodeBitString :: HuffmanTreeNode -> BitString -> BS.ByteString
decodeBitString huffmanTree bits = BS.reverse . 
                                   decodedBytes . 
                                   readUntilEmpty $ StreamState bits BS.empty huffmanTree huffmanTree

                                   
{-
data StreamState
    
    A type to store the state of the parsing/decoding process.
    
    remainingBits : the bits sequence still to be decoded.
    decodedBytes  : the sequence of bytes produced so far by decoding.
    rootNode      : the root node of the huffman tree.
    curNode       : the node of the huffman tree that corresponds to 
                    the last bit that was read.
-}
data StreamState = StreamState {
      remainingBits :: BitString
    , decodedBytes  :: BS.ByteString
    , rootNode :: HuffmanTreeNode
    , curNode  :: HuffmanTreeNode
} deriving (Show,Eq)


{-
readNextBit :: StreamState -> StreamState

    Given a stream state, read the next bit and return the new state.
    
    If as a result of reading the bit, a leaf huffman tree node was
    reached, append the associated byte to decodedBytes.
    
    Otherwise if an internal node was reached, updated the remainingBits.
-}
readNextBit :: StreamState -> StreamState
readNextBit (StreamState [] _ _ _) = error "cannot read from empty stream"
readNextBit (StreamState (nextBit:xs) bytes rootNode curNode) = 
    case getNextNode curNode nextBit of
        InternalNode _ _ _  -> 
        -- Internal node: update the stream state and continue
            StreamState xs bytes rootNode (getNextNode curNode nextBit)
        
        LeafNode i c        -> 
        -- Leaf node reached: append the symbol that corresponds to the node to the decoded bytes
        -- or return if the encountered symbol is the endOfStream
            if c == endOfStream then StreamState [] bytes rootNode rootNode             -- eof encountered, set remainingBits to []
            else StreamState xs (prependSymbol c bytes) rootNode rootNode  -- convert symbol to char and append to decodedChars
    
    where   getNextNode :: HuffmanTreeNode -> Bool -> HuffmanTreeNode
            getNextNode curNode nextBit = 
                case curNode of 
                    InternalNode i n1 n2 -> if nextBit == False then n1 else n2
                    _                    -> error "reached leaf node: no next node"
                
            prependSymbol :: Symbol -> BS.ByteString -> BS.ByteString
            prependSymbol s bs = (BS.singleton $ int_to_word8 s) `BS.append` bs


{-
readUntilEmpty :: StreamState -> StreamState
    
    Reads recursively the input bit sequence, assembling the result as it progresses.
-}            
readUntilEmpty :: StreamState -> StreamState
readUntilEmpty (StreamState [] b r c) = StreamState [] b r c
readUntilEmpty curState = readUntilEmpty $ readNextBit curState


{-
encodeByteString :: CodeMap -> BS.ByteString -> BitString

    Using the given map of encodings, generate a bit sequence
    by mapping each input byte to its encoding.
-}
encodeByteString :: CodeMap -> BS.ByteString -> BitString
encodeByteString codeMap bs = reverse .     -- because of encodeByteStringImpl's implementaion, the bitstring must be reversed
                              snd . encodeByteStringImpl codeMap $ (bs, [])


{-
encodeByteStringImpl 
    :: CodeMap                     -- The map of encodings
    -> (BS.ByteString, BitString)  -- (Bytes to encode, Bit sequence to append to)
    -> (BS.ByteString, BitString)  -- (remaining bytes, new bit sequence)
    
    Using the given map of encodings, encode each byte to the corresponding encoding
    and append the resulting bits to the bit sequence.   
-}
encodeByteStringImpl :: CodeMap -> (BS.ByteString, BitString) -> (BS.ByteString, BitString)
encodeByteStringImpl codeMap (bytes, bits) =
    if BS.null bytes then
        (BS.empty, bits)
    else
        let firstByteEncoded = encodeSymbol codeMap . word8_to_int $ BS.head bytes
            bits' =  (reverse firstByteEncoded) ++ bits -- Adding at the beggining is much faster
                                                        -- The bit string will have to be reversed though.
            bytes' = BS.tail bytes
        in  encodeByteStringImpl codeMap (bytes', bits')

{-
encodeSymbol :: CodeMap -> Symbol -> BitString
    
    Return the bit sequence that the given symbol should be encoded to.
-}
encodeSymbol :: CodeMap -> Symbol -> BitString
encodeSymbol codeMap s = codeMap ! s


{- 
createHeader :: [ByteFrequency] -> BS.ByteString

    Serialize the [ByteFrequency] list to a byte string.
    The [(Word8, Int)] list is stored as a sequence of (Word8,Word64) pairs,
    where Word8 represents the symbol and Word64 its frequency.
    A pair of 0,0 is appended to mark the end of byte-frequency pairs.
-}
createHeader :: [ByteFrequency] -> BS.ByteString
createHeader frequencies =  
    let frequencies' = (++[(0,0)]) . filter (\(x,i)-> i>0) $ frequencies  -- keep symbols whose frequency is greater than 0
                                                                          -- append (0,0) to mark the end.
    in  Bin.toLazyByteString $ foldl' appendByteFrequency Bin.empty frequencies'

    where   appendByteFrequency :: Bin.Builder -> (Word8,Int) -> Bin.Builder
            appendByteFrequency builder (byte, frequency) = 
                let b_w8     = Bin.singleton byte                            -- byte as binary builder
                    b_freq64 = Bin.putWord64le $ int_to_word64 frequency     -- frequency as binary builder
                in  Bin.append (Bin.append builder b_w8) b_freq64            -- append the byte and then its frequency.

                
{- 
HuffmanTreeNode

    Represents a node of the huffman tree.
    A node can be either internal or leaf.
-}
data HuffmanTreeNode = LeafNode Frequency Symbol | InternalNode Frequency HuffmanTreeNode HuffmanTreeNode deriving (Show, Eq)                


{-
generateHuffmanCodeMap :: [SymbolFrequency] -> (CodeMap, HuffmanTreeNode)

    Given a list of symbol-frequency pairs generates a huffman tree and 
    and the map of symbol->encoding pairs.
-}
generateHuffmanCodeMap :: [SymbolFrequency] -> (CodeMap,HuffmanTreeNode)
generateHuffmanCodeMap [] = error "cannot calculate huffman codes from empty sequence" -- trivial case, no characters given
generateHuffmanCodeMap symbolFrequencies = 
    let compareSymbolsFunc s1 s2
            | (snd s1)  < (snd s2) = LT
            | (snd s1) == (snd s2) && (fst s1)  < (fst s2) = LT
            | (snd s1) == (snd s2) && (fst s1) == (fst s2) = EQ
            | otherwise = GT
        
        sortedFrequencies = filter (\(x,i) -> i>0) $ sortBy compareSymbolsFunc symbolFrequencies
        symbolToTreeNode  = (\(s,i) -> LeafNode i s)
        huffmanTree       = buildHuffmanTree . map symbolToTreeNode $ sortedFrequencies
        encodings         = getEncodings huffmanTree
        codeMap           = foldl' writeCodeMap (listArray (0,256) []) encodings
    in (codeMap, huffmanTree)
    
    where   writeCodeMap :: CodeMap -> SymbolEncoding -> CodeMap
            writeCodeMap m (s,e) = m // [(s,e)]
                
            
{- 
buildHuffmanTree :: [HuffmanTreeNode] -> HuffmanTreeNode

    Implementation of the huffman algorithm. Given a list of nodes,
    it builds the huffman tree and returns its root.
-}            
buildHuffmanTree :: [HuffmanTreeNode] -> HuffmanTreeNode
buildHuffmanTree [] = error "cannot generate tree from empty list of nodes"
buildHuffmanTree [x] = x 
buildHuffmanTree (n1:n2:rest) = buildHuffmanTree $ insertInOrder rest $ mergeTreeNodes n2 n1

            
{-
getFrequency :: HuffmanTreeNode -> Int

    Convenience function
-}
getFrequency :: HuffmanTreeNode -> Int
getFrequency (LeafNode frequency _) = frequency
getFrequency (InternalNode frequency _ _) = frequency


{-
mergeTreeNodes :: HuffmanTreeNode -> HuffmanTreeNode -> HuffmanTreeNode

    Convenience function
-}
mergeTreeNodes :: HuffmanTreeNode -> HuffmanTreeNode -> HuffmanTreeNode
mergeTreeNodes n1 n2 = InternalNode (f1+f2) n1 n2
    where   f1 = getFrequency n1
            f2 = getFrequency n2
         
{-
getEncodings :: HuffmanTreeNode -> [SymbolEncoding]

    Given a huffman tree, generate and return the list of 
    symbol -> encoding associations.
-}
getEncodings :: HuffmanTreeNode -> [SymbolEncoding]
getEncodings (LeafNode f c) = [(c,[])]
getEncodings (InternalNode f n1 n2) = leftTreeElems ++ rightTreeElems
    where leftTreeElems  = map (prepend False) $ (getEncodings n1)   -- prepend False to all left tree elements
          rightTreeElems = map (prepend True)  $ (getEncodings n2)   -- prepend True to all right tree elements
          prepend x encoding = (fst encoding, x : (snd encoding))    -- prepends x to the BitString part of the encoding tupple

{-
insertInOrder :: [HuffmanTreeNode] -> HuffmanTreeNode -> [HuffmanTreeNode]

    Insert a node to a list of nodes, producing a list sorted by frequency.
-}          
insertInOrder :: [HuffmanTreeNode] -> HuffmanTreeNode -> [HuffmanTreeNode]
insertInOrder xs x = sortNodesByFrequency $ xs ++ [x]


{-
sortNodesByFrequency :: [HuffmanTreeNode] -> [HuffmanTreeNode]

    Sort a list of huffman tree nodes by frequency.
-}
sortNodesByFrequency :: [HuffmanTreeNode] -> [HuffmanTreeNode]
sortNodesByFrequency xs = sortBy compareNodesFunc xs
    where compareNodesFunc n1 n2
            | getFrequency n1 < getFrequency n2 = LT
            | getFrequency n1 > getFrequency n2 = GT
            | otherwise = EQ


type ByteFrequencyMap = DMap.Map Word8 Int

{-
countBytes :: BS.ByteString -> ByteFrequencyMap

    Counts the occurences of each value in [0 .. 255] in the given bytestring.
    The result is represented as a map.
-}
countBytes :: BS.ByteString -> ByteFrequencyMap
countBytes bs = BS.foldl' incrementCount DMap.empty bs 
    where   incrementCount :: ByteFrequencyMap -> Word8 -> ByteFrequencyMap
            incrementCount frequencyMap word =  
                let curCount = DMap.findWithDefault 0 word frequencyMap
                in  DMap.insert word (curCount+1) frequencyMap


{- 
toByteString :: BitString -> BS.ByteString

    Converts the given bit string to a byte string, padding with zeros
    at the end if necessary.
-}
toByteString :: BitString -> BS.ByteString
toByteString bits = BS.reverse $ toByteStringImpl BS.empty bits


{-
toByteStringImpl 
    :: BS.ByteString    -- the byte string to append to
    -> BitString        -- the bit sequence to read from
    -> BS.ByteString    -- the resulting byte string
    
    Recursively converts the given bit sequence to a byte string,
    reading 8 bits at a time from the beginning of the bit sequence.
    Bit sequence is padded with zeros in the end if its length is 
    not multiple of 8.
-}
toByteStringImpl :: BS.ByteString -> BitString -> BS.ByteString
toByteStringImpl accumulator []   = accumulator
toByteStringImpl accumulator bits = toByteStringImpl accumulator' remainingBits
    where first8Bits    = take 8 $ bits ++ (repeat False)   -- padded with zeros if less than 8 bits
          firstByte     = bitsToChar first8Bits
          remainingBits = drop 8 bits
          accumulator'  = (firstByte `BS.cons` accumulator) -- Add byte at the beginning of the byte string, instead of appending,
                                                            -- for efficiency. The result will have to be reversed.

                                                            
{-
fromByteString :: BS.ByteString -> BitString

    Creates a bitstring from the given bytestring.
-}
fromByteString :: BS.ByteString -> BitString
fromByteString bs = reverse $ fromByteStringImpl [] bs          
          
          
{-
fromByteStringImpl :: BitString -> BS.ByteString ->BitString

    Recursively creates a bit string from the given bytestring.
-}          
fromByteStringImpl :: BitString -> BS.ByteString ->BitString
fromByteStringImpl accumulator bytes = 
    if bytes == BS.empty then  accumulator
    else    
        let bits = reverse . word8ToBitstring $ BS.head bytes
            accumulator' =  bits ++ accumulator
            bytes' = BS.tail bytes
        in  fromByteStringImpl accumulator' bytes'

{-
bitsToChar :: [Bool] -> Word8

    Converts the given bit sequence to a Word8.
    Input bit sequence is expected to always be 8 bits long.
-}        
bitsToChar :: [Bool] -> Word8
bitsToChar bits = int_to_word8 . foldl' (\acc b -> (acc*2) + (toInt b)) 0 $ bits
    where   toInt x = if x then 1 else 0


{-
word8ToBitstring :: Word8 -> [Bool]
    
    Converts the given word8 to a bit string.
-}    
word8ToBitstring :: Word8 -> [Bool]
word8ToBitstring word = snd $ foldl' (\(w, bits) pow2 -> if w >= pow2 then (w-pow2, bits++[True])
                                                         else (w,     bits++[False])) 
                                     (word,[])
                                     [128,64,32,16,8,4,2,1]