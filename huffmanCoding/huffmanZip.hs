import System.Environment
import System.IO
import Control.Monad
import System.CPUTime
import Text.Printf
import qualified Data.ByteString.Lazy as BS
import qualified HuffmanCompression as HC


huffmanCompress :: String -> String -> IO()
huffmanCompress source dest = do
    contents <- BS.readFile source
    let compressed = HC.compressByteString contents
    BS.writeFile dest compressed

huffmanDecompress :: String -> String -> IO()
huffmanDecompress source dest = do
    contents <- BS.readFile source
    let decompressed = HC.decompressByteString contents
    case decompressed of 
        Nothing -> error $ "Cannot decompress file: " ++ source
        Just content -> BS.writeFile dest content

printUsageInfo :: IO ()
printUsageInfo = do
    putStrLn "Wrong number of arguments:"
    putStrLn "to compress use  :   huffmanZip -c <souce file> <output file>"
    putStrLn "to decompress use:   huffmanZip -d <souce file> <output file>"
    
    
main :: IO ()
main = do
    args <- getArgs
    if length args < 3 then
        printUsageInfo
    else do
        let mode = args!!0
        let source = args!!1
        let dest = args !! 2
        
        if mode=="-c" || mode=="-d" then do
            start <- getCPUTime
            let f = if mode=="-c" then huffmanCompress else huffmanDecompress
            f source dest
            end   <- getCPUTime
            let diff = (fromIntegral (end - start)) / (10^12)
            printf  "Computation time: %0.3f sec\n" (diff :: Double)
        else
            printUsageInfo
